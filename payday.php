<?php
function getData()
{
    // simple overwrite - this year December payday is on the 18th not 25th
    $overwrite = [
        '1608854400' => '1608249600'
    ];
    // get day of the month (without leading zero)
    $today_day = date('j');

    // if today is after 25 calculate next month 25ht insted
    if ((int)$today_day < 25) {
        $next_payday = mktime(0, 0, 0, date("m"), 25, date("Y"));

    } else {
        $next_payday = mktime(0, 0, 0, date("m") + 1, 25, date("Y"));
    }

    if(isset($overwrite[$next_payday])) {
        $next_payday = $overwrite[$next_payday];
    }

    // if 25th is a weekend move back to previous Friday
    $payday_weekday = date('N', $next_payday);
    if ((int)$payday_weekday == 6) {
        $next_payday -= 86400;
    } elseif ((int)$payday_weekday == 7) {
        $next_payday -= 172800;
    }

    $next_day = mktime(0, 0, 0, date("m"), date('j') + 1, date("Y"));
    $days_left = 1;
    $days_count = [0, 0, 0, 0, 0, 0, 0];

    while ($next_day < $next_payday) {
        $weekday = (int)date('w', $next_day);
        // this lines counts number of every week days
        $days_count[$weekday]++;
        // add one day in sec.
        $next_day += 86400;
        // increment days counter by one
        $days_left++;
    }


    $data = [];

    // print the result in a very messy way :)
    $ds = $days_left     > 1 ? 's' : '';
    $ws = $days_count[6] > 1 ? 's' : '';

    $data['{payday_date}']   = date('l  M dS', $next_payday);
    $data['{weekends_left}'] = $days_count[6] . ' weekend' . $ws . ' left';
    $data['{days_left}']     = $days_left . ' day' . $ds;

    $s = ($days_count[1] > 1) ? 's' : '';
    $data['{mon}'] = $days_count[1] . ' Monday' . $s;

    $s = ($days_count[2] > 1) ? 's' : '';
    $data['{tue}'] = $days_count[2] . ' Tuesday' . $s;

    $s = ($days_count[3] > 1) ? 's' : '';
    $data['{wed}'] = $days_count[3] . ' Wednesday' . $s;

    $s = ($days_count[4] > 1) ? 's' : '';
    $data['{thu}'] = $days_count[4] . ' Thursday' . $s;

    $s = ($days_count[5] > 1) ? 's' : '';
    $data['{fri}'] = $days_count[5] . ' Friday' . $s;

    $s = ($days_count[6] > 1) ? 's' : '';
    $data['{sat}'] = $days_count[6] . ' Saturday' . $s;

    $s = ($days_count[0] > 1) ? 's' : '';
    $data['{sun}'] = $days_count[0] . ' Sunday' . $s;

    return $data;
}

function render()
{
    $data = getData();
    $template = '<div class="card mb-4 shadow-sm text-light bg-secondary">
      <div class="card-header">
        <h4 class="my-0 font-weight-normal">{payday_date}</h4>
      </div>
      <div class="card-body">
        <h1 class="display-3">{days_left}</h1>
        <h3>{weekends_left}</h3>
        <ul class="list-unstyled mt-3 mb-4">
          <li>{mon}</li>
          <li>{tue}</li>
          <li>{wed}</li>
          <li>{thu}</li>
          <li>{fri}</li>
          <li>{sat}</li>
          <li>{sun}</li>
        </ul>
      </div>
    </div>';

    echo strtr($template, $data);
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Payday</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
          integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <style>
    </style>
</head>
<body class="bg-dark">
<div class="container bg-dark">
    <div class="card-deck mt-3 mb-3 text-center">
        <?php render(); ?>
    </div>
</div>
</body>
</html>