function git-update-from-develop () {
   BRANCH_NAME=$(git rev-parse --abbrev-ref HEAD)
   if [ $BRANCH_NAME = "master" ]; then
      echo "on master branch ... exit"
      return 0
   elif [ $BRANCH_NAME = "develop"  ]; then
      echo "on develop branch ... exit"
      return 0
   fi

   git pull --all
   git checkout $BRANCH_NAME
   #git merge --no-ff develop
   git merge develop

   if [ "$1" == "-p" ]; then
     echo "push ..."
     git push
   fi
}