<?php

function renderCalc($year = false)
{
    if(!$year) {
        $year = date('Y');
    }
    $str = '';
    for($m = 1; $m <= 12; $m++) {
        $date = getLastWorkingDay($m, $year);
        $start = date('Ymd', $date);
        $stop  = date('Ymd', strtotime(date('Y-m-d', $date) . ' +1 day'));
        $str .= 'BEGIN:VEVENT
DTSTART;VALUE=DATE:'.$start.'
DTEND;VALUE=DATE:'.$stop.'
DTSTAMP:20211109T221407Z
CREATED:20211109T221245Z
UID:'.uniqid().'
DESCRIPTION:
LOCATION:
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:PayDay
TRANSP:TRANSPARENT
END:VEVENT' . "\n";
    }
    return $str;
}

function getLastWorkingDay($month, $year)
{

    $d           = mktime(0, 0, 0, $month, 1, $year);
    $next_payday = strtotime(date('Y-n-t', $d));
    $week_day    = date('N', $next_payday);
    if($week_day == '6') {
        $next_payday = $next_payday - 86400;
    } elseif($week_day == '7') {
        $next_payday = $next_payday - 172800;
    }
    //date('Ymd', $next_payday);
    return $next_payday;
}

$year = date('Y');

if(!empty($_GET['year'])) {
    $year = (int) $_GET['year'];
    if($year < 2021 || $year > 2035) {
        $year = date('Y');
    }
}

header('Content-Type: text/calendar');
header('Content-Disposition: attachment; filename="Gcal_' . $year . '.ics";');
$a = 'BEGIN:VCALENDAR
PRODID:-//Google Inc//Google Calendar 70.9054//EN
VERSION:2.0
CALSCALE:GREGORIAN
METHOD:PUBLISH
X-WR-CALNAME:PayDay Calendar
X-WR-TIMEZONE:Europe/London
X-WR-CALDESC:Description goes here' . "\n";
$a .= renderCalc($year);
$a .= 'END:VCALENDAR';
echo $a;